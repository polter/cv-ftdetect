#-------------------------------------------------
#
# Project created by QtCreator 2015-11-26T12:27:13
#
#-------------------------------------------------

QT -= core gui

TARGET = ftdetect

SOURCES += main.cpp

LIBS += -lopencv_core -lopencv_highgui -lopencv_imgproc -lopencv_features2d -lopencv_calib3d -L/home/polter/Downloads/vlfeat-0.9.20/bin/glnxa64/ -lvl
INCLUDEPATH += /home/polter/Downloads/vlfeat-0.9.20/

QMAKE_CXXFLAGS += -fopenmp
QMAKE_LFLAGS += -fopenmp

QMAKE_CXXFLAGS_RELEASE += -O3
QMAKE_CXXFLAGS_RELEASE -= -O2
