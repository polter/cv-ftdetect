#include <vector>
#include <utility>
#include <fstream>

#include <opencv2/opencv.hpp>
#include <vl/sift.h>

using namespace std;
using namespace cv;

const char *window_name = "Feature detection";
const int descr_size = 128;
const int max_scale = 300;
const double max_diff = 0.4;

int feature_no = 0;

// My ShiTomasi parameters
int max_corners = 100;
int min_distance = 5;
int quality_level = 100;
int block_size = 3;
int use_harris = 0;

const char *descnames[] = {
    "sift", "my_shitomasi", "harris", "fast", "shitomasi", "mser", "star"
};



int sift_detect_descriptors(const Mat &image, Mat_<float> &descr, vector<KeyPoint> &keypoints,
                            int octaves_num, int levels_per_octave, int octave_min,
                            double edge_threshold, double peak_threshold)
{
    VlSiftFilt *filt;
    VlSiftKeypoint const *keys;
    vl_bool err;
    vl_sift_pix *data;
    int first = 1;
    int saved_keys_num = 0;
    double magnif;

    data = new vl_sift_pix[image.total()];
    for (size_t i = 0; i < image.total(); i++) {
        data[i] = image.data[i] / 255.0;
    }

    filt = vl_sift_new(image.cols, image.rows, octaves_num, levels_per_octave, octave_min);
    vl_sift_set_edge_thresh (filt, edge_threshold);
    vl_sift_set_peak_thresh (filt, peak_threshold);

    while (true) {
        int nkeys;

        // Process octave
        if (first) {
            first = 0;
            err = vl_sift_process_first_octave (filt, data);
        } else {
            err = vl_sift_process_next_octave (filt);
        }

        if (err) {
            err = 0;
            break;
        }

        vl_sift_detect (filt);

        keys  = vl_sift_get_keypoints (filt);
        nkeys = vl_sift_get_nkeypoints (filt);
        magnif = vl_sift_get_magnif(filt);

        // For each keypoint
        for (int i = 0; i < nkeys; ++i) {
            double angles[4];
            int nangles;
            VlSiftKeypoint const *k;

            // Obtain keypoint orientations
            k = keys + i;
            nangles = vl_sift_calc_keypoint_orientations(filt, angles, k);

            if (nangles < 1)
                angles[0] = 0;

            vl_sift_pix buf[descr_size];
            vl_sift_calc_keypoint_descriptor(filt, buf, k, angles[0]);
            descr.push_back(Mat_<float>(1, descr_size, buf).clone());

            // Convert to OpenCV KeyPoint structure
            KeyPoint kp;
            kp.pt.x = k->x;
            kp.pt.y = k->y;
            kp.angle = angles[0];
            kp.size = k->sigma * magnif;
            keypoints.push_back(kp);
            saved_keys_num++;
        }
    }

    vl_sift_delete(filt);
    delete[] data;

    return saved_keys_num;
}



void sift_descriptors_from_keypoints(const Mat &image,
                                     const vector<KeyPoint> &keypoints, Mat_<float> &descriptors)
{
    // Convert from Mat to vl_sift_pix *
    vl_sift_pix *data = new vl_sift_pix[image.total()];
    for (size_t i = 0; i < image.total(); i++) {
        data[i] = image.data[i] / 255.0;
    }

    VlSiftFilt *sift = vl_sift_new(image.cols, image.rows, 1, 1, 0);
    vl_sift_process_first_octave(sift, data);

    // Loop through our keypoints
    vl_sift_pix descr[descr_size];
    for (vector<KeyPoint>::const_iterator i = keypoints.begin(),
         end = keypoints.end(); i != end; ++i) {
        double angles[4];
        VlSiftKeypoint kp;

        // Convert OpenCV KeyPoint to vlfeat VlSiftKeypoint
        vl_sift_keypoint_init(sift, &kp, (*i).pt.x, (*i).pt.y, sift->sigma0);
        int nangles = vl_sift_calc_keypoint_orientations(sift, angles, &kp);
        if (nangles < 1)
            angles[0] = 0;

        // Calculate and save descriptor
        vl_sift_calc_keypoint_descriptor(sift, descr, &kp, angles[0]);
        descriptors.push_back(Mat_<float>(1, descr_size, descr).clone());

        // Draw keypoints with orientations
        /*circle(frame, Point(kp.x, kp.y), 10, Scalar(0, 255, 0), 1, CV_AA);

        for (int j = 0; j < nangles; j++) {
            double alpha =  angles[j];

            Point p1, p2, p3;
            p1.x = kp.x;
            p1.y = kp.y;

            p2.x = kp.x + 10;
            p2.y = kp.y;

            p3.x = cos(alpha) * 10 + kp.x;
            p3.y = -sin(alpha) * 10 + kp.y;

            Scalar color = (j == 0) ? Scalar(0, 0, 255) : Scalar(255, 0, 0);
            line(frame, Point(kp.x, kp.y), p3, color, 1, CV_AA);
        }*/
    }

    vl_sift_delete(sift);
    delete[] data;
}



void rotate(Mat &src, double angle, Mat &dst)
{
    Point2f pt(src.cols / 2.0, src.rows / 2.0);
    Mat r = getRotationMatrix2D(pt, angle, 1.0);

    Rect box = RotatedRect(pt, src.size(), angle).boundingRect();
    // adjust transformation matrix
    r.at<double>(0,2) += box.width / 2.0 - pt.x;
    r.at<double>(1,2) += box.height / 2.0 - pt.y;

    warpAffine(src, dst, r, box.size());
}



int main(int argc, char *argv[])
{
    // Check number of arguments.
    if (argc < 2) {
        cout << "usage: " << argv[0] << " <image path>" << endl;
        return EXIT_FAILURE;
    }

    Mat image_src = imread(argv[1]);
    if (image_src.empty()) {
        cout << "Invalid image \"" << argv[1] << "\", stopping!" << endl;
        return EXIT_FAILURE;
    }

    Ptr<FeatureDetector> detectors[5];
    detectors[0] = FeatureDetector::create("HARRIS");
    detectors[1] = FeatureDetector::create("FAST");
    detectors[2] = FeatureDetector::create("GFTT");
    detectors[3] = FeatureDetector::create("MSER");
    detectors[4] = FeatureDetector::create("STAR");

    namedWindow(window_name, WINDOW_AUTOSIZE);
    createTrackbar("0 - sift\n" \
                   "1 - my gftt\n" \
                   "2 - harris\n" \
                   "3 - fast\n" \
                   "4 - gfft\n" \
                   "5 - mser\n" \
                   "6 - star",
                   window_name, &feature_no, 4);

    createTrackbar("Max corners",   window_name, &max_corners,   100);
    createTrackbar("Min distance",  window_name, &min_distance,  100);
    createTrackbar("Quality level", window_name, &quality_level, 1000);
    createTrackbar("Block size",    window_name, &block_size,    20);
    createTrackbar("Use Harris",    window_name, &use_harris,    1);

    vector<Mat> images;
    images.push_back(image_src);

    // Rotate 181 times
    for (int i = -90; i <= 90; i++) {
        //if (i == 0) continue;

        Mat rotated;
        rotate(image_src, i, rotated);
        images.push_back(rotated);
    }

    // Scale max_scale times
    for (int i = max_scale; i > 0; i--) {
        Mat scaled;
        resize(image_src, scaled, Size(image_src.cols * i / 100,
                                       image_src.rows * i / 100));
        images.push_back(scaled);
    }

    // Arrays of descriptors and keypoints from all images
    vector< Mat_<float> > descriptors(images.size());
    vector< vector<KeyPoint> > keypoints(images.size());

    int count = 0;
#pragma omp parallel for schedule(static, 10)
    for (size_t i = 0; i < images.size(); ++i) {
        Mat frame = images[i], frame_tmp;

        cvtColor(frame, frame_tmp, CV_RGB2GRAY);

        // Arrays of descriptors and keypoints from current image
        Mat_<float> frame_descriptors;
        vector<KeyPoint> frame_keypoints;

        if (feature_no == 0) { // Feature 0 - SIFT
            sift_detect_descriptors(frame_tmp, frame_descriptors, frame_keypoints,
                                    3, 8, 0, 10, 0.01);

            keypoints[i] = frame_keypoints;
            descriptors[i] = frame_descriptors;
        } else {
            if (feature_no == 1) { // Feature 1 - GFFT
                vector<Point2f> features;
                goodFeaturesToTrack(frame_tmp, features, max_corners,
                                    max(quality_level, 1) / 10000.0, max(min_distance, 1),
                                    Mat(), max(block_size, 1), use_harris);

                frame_keypoints.reserve(features.size());
                for (vector<Point2f>::iterator i = features.begin(),
                     end = features.end(); i != end; ++i) {
                    frame_keypoints.push_back(KeyPoint(*i, 1));
                }
            } else { // Otherwise pick detector from array
                Ptr<FeatureDetector> detector = detectors[feature_no - 2];
                detector->detect(frame_tmp, frame_keypoints);
            }

            sift_descriptors_from_keypoints(frame_tmp, frame_keypoints, frame_descriptors);

            keypoints[i] = frame_keypoints;
            descriptors[i] = frame_descriptors;
        }

        cout << "processed " << ++count << " of " << images.size() << endl;
    }

    // Calculate repeatability and save percentages to this arrays
    float key_rotations[181 * 2], desc_rotations[181 * 2];
    float key_scales[max_scale * 2], desc_scales[max_scale * 2];
    float metric_rotations[181 * 2], metric_scales[max_scale * 2];

    // Start from 1 because descriptors of source image placed at begining
    for (size_t i = 1; i < descriptors.size(); i++) {
        Mat_<float> good_descr_obj, good_descr_src;
        vector<KeyPoint> good_keypts_obj, good_keypts_src;

        Point2f center;
        double angle = 0;
        double scale = 1;

        Mat_<double> tr;
        int num = i - 1;
        if (num < 181) {
            angle = num - 90.0;
            center = Point2f(images[0].cols / 2.0, images[0].rows / 2.0);

            Rect box = RotatedRect(center, images[0].size(), angle).boundingRect();

            /*Mat_<float> r = getRotationMatrix2D(center, angle, scale);

            Rect box = RotatedRect(center, images[0].size(), angle).boundingRect();
            r.at<double>(0,2) += box.width / 2.0 - center.x;
            r.at<double>(1,2) += box.height / 2.0 - center.y;

            invertAffineTransform(r, tr);*/

            tr = getRotationMatrix2D(center, angle, scale);
            tr.at<double>(0,2) += box.width / 2.0 - center.x;
            tr.at<double>(1,2) += box.height / 2.0 - center.y;

            invertAffineTransform(tr, tr);

            /*Mat res;
            warpAffine(images[i], res, tr, images[0].size());
            imshow("D", res);*/
        } else {
            scale = 1.0 / ((max_scale + 181.0 - num) / 100.0);
            center = Point2f(0, 0);
            tr = getRotationMatrix2D(center, angle, scale);
            //invertAffineTransform(tr, tr);
        }

        //Mat_<float> dist(keypoints[i].size(), keypoints[0].size());


        /*for (size_t j = 0; j < keypoints[i].size(); j++) {
            Mat_<double> src(3, 1), dst(2, 1);
            src << keypoints[i][j].pt.x, keypoints[i][j].pt.y, 1.0;
            src = tr * src;

            double min_dist = DBL_MAX;
            int closest_key = 0;
            for (size_t k = 0; k < keypoints[0].size(); k++) {
                dst << keypoints[0][k].pt.x, keypoints[0][k].pt.y;
                double d = norm(src, dst);
                if (d < min_dist) {
                    min_dist = d;
                    closest_key = k;
                }
                //dist.ptr<float>(j)[k] = d;
            }

            if (min_dist < 1) {
                good_descr_obj.push_back(descriptors[i].row(j));
                good_descr_src.push_back(descriptors[0].row(closest_key));

                good_keypts_obj.push_back(keypoints[i][j]);
                good_keypts_src.push_back(keypoints[0][closest_key]);
            }
        }*/

        //cout << dist << endl;


        BFMatcher matcher;
        vector<DMatch> matches;
        double max_dist = 0; double min_dist = 100;

        //if (!good_descr_obj.empty()) {
            //matcher.match(good_descr_src, good_descr_obj, matches);
        if (!descriptors[i].empty())
            matcher.match(descriptors[0], descriptors[i], matches);
            // Caculate of max and min distances between keypoints
            /*for (int j = 0; j < descriptors[0].rows; j++) {
                 double dist = matches[j].distance;
                 if (dist < min_dist) min_dist = dist;
                 if (dist > max_dist) max_dist = dist;
             }*/
        //}

        /*cout << "Max dist: " <<  max_dist << endl;
        cout << "Min dist: " << min_dist << endl;*/

        vector<DMatch> good_matches;
        vector<Point2f> obj_points;
        vector<Point2f> scene_points;

        vector<int> matched_ind;

        int TP = 0, FP = 0;
        // Loop through matches and calculate repeatability
        for (vector<DMatch>::iterator j = matches.begin(),
                     end = matches.end(); j != end; j++) {
            DMatch &match = *j;
           // if (match.distance <= max(3 * min_dist, 0.02)) {
                //Point point_scene = good_keypts_src[match.queryIdx].pt;
                //Point point_obj   = good_keypts_obj[match.trainIdx].pt;
                Point point_scene = keypoints[0][match.queryIdx].pt;
                Point point_obj   = keypoints[i][match.trainIdx].pt;

                Mat_<double> src(3, 1), dst(2, 1);
                src << point_obj.x, point_obj.y, 1.0;
                dst << point_scene.x, point_scene.y;
                src = tr * src;

                double dist = norm(src, dst);

                if (dist < max_diff) {
                    TP++;
                    //cout << i << " " << dist << endl;

                    good_matches.push_back(match);
                    scene_points.push_back(point_scene);
                    obj_points.push_back(point_obj);
                } else {
                    FP++;
                }

                /*for (size_t k = 0; k < unmatched.size(); k++) {
                    if (unmatched[k].pt.x == point_obj.x && unmatched[k].pt.y == point_obj.y) {
                        unmatched.erase(unmatched.begin() + k);
                        cout << "FOYUND" << endl;
                        break;
                    }
                }*/
                matched_ind.push_back(match.trainIdx);
           //}
        }

        int FN = 0;
        for (size_t j = 0; j < keypoints[i].size(); j++) {
            bool found = false;
            for (size_t k = 0; k < matched_ind.size(); k++) {
                if (matched_ind[k] == j) {
                    found = true;
                    break;
                }
            }
            if (found) continue;

            Mat_<double> src(3, 1), dst(2, 1);
            src << keypoints[i][j].pt.x, keypoints[i][j].pt.y, 1.0;
            src = tr * src;

            double min_dist = DBL_MAX;
            int closest_key = 0;
            for (size_t k = 0; k < keypoints[0].size(); k++) {
                dst << keypoints[0][k].pt.x, keypoints[0][k].pt.y;
                double d = norm(src, dst);
                if (d < min_dist) {
                    min_dist = d;
                    closest_key = k;
                }
            }

            if (min_dist < max_diff) {
                FN++;
            }
        }

        //cout << "matches: " << matches.size() << " good: " << good_matches.size() << endl;

        /*int TN = keypoints[0].size() - good_keypts_obj.size(); // geometry not ok = TN
        int FN = good_keypts_obj.size() - good_matches.size();      // geometry ok, but not match = FN
        int TP = good_matches.size();                          // geometry ok and match = TP
        int FP = matches.size() - good_matches.size();      // geometry not ok, but match = FP*/

        float precision = (TP == 0 || (TP == 0 && FP == 0)) ? 0 : TP * 1.0 / (TP + FP);
        float recall    = (TP == 0 || (TP == 0 && FN == 0)) ? 0 : TP * 1.0 / (TP + FN);
        float f_metric  = (precision == 0 || recall == 0) ? 0 : 2 * precision * recall / (precision + recall);

        cout << "precision:" << precision << " recall:" << recall << " TP:" << TP << " FP:" << FP << " FN:" << FN << " (" <<  good_keypts_obj.size() <<" - " << good_matches.size() << ")" << endl;

        // We can build homography from 4 points min.
        if (obj_points.size() >= 4) {
            // Build homography matrix
            Mat H = findHomography(obj_points, scene_points, CV_RANSAC);

            // Use the Homography Matrix to warp the images
            Mat image_warped;
            warpPerspective(images[i], image_warped, H, Size(images[0].cols, images[0].rows));
            imshow("Warped", image_warped);

            cout << "WARP " << (max_scale + 181 - num) << endl;
        }

        // Draw matches on main window
        Mat image_matches;
        /*drawMatches(image_src, good_keypts_src, images[i], good_keypts_obj,
                good_matches, image_matches, Scalar::all(0));*/
        drawMatches(image_src, keypoints[0], images[i], keypoints[i],
                        good_matches, image_matches, Scalar::all(0));
        imshow(window_name, image_matches);

        // Is no matches found, set 0%, otherwise calculate percentage
        float key_percentage = keypoints[i].empty() ? 0 : (good_keypts_obj.size() * 1.0 / keypoints[i].size()) * 100.0;
        float match_percentage = matches.empty() ? 0 : (good_matches.size() * 1.0 / matches.size()) * 100.0;

        waitKey(1);
        if (num < 181) {
            // Write parameters of rotated images
            key_rotations[2 * (i - 1)] = num - 90;
            key_rotations[2 * (i - 1) + 1] = key_percentage;

            desc_rotations[2 * (i - 1)] = num - 90;
            desc_rotations[2 * (i - 1) + 1] = match_percentage;

            metric_rotations[2 * (i - 1)] = num - 90;
            metric_rotations[2 * (i - 1) + 1] = f_metric;
            // cout << "rotations[" << ((i - 1)) << "] = " << (num - 90) << endl;
        } else {
            // Write parameters of scaled images
            num = max_scale + 181 - num;
            key_scales[2 * (i - 1 - 181)] = num;
            key_scales[2 * (i - 1 - 181) + 1] = key_percentage;

            desc_scales[2 * (i - 1 - 181)] = num;
            desc_scales[2 * (i - 1 - 181) + 1] = match_percentage;

            metric_scales[2 * (i - 1 - 181)] = num;
            metric_scales[2 * (i - 1 - 181) + 1] = f_metric;
            // cout << "scaled[" << ((i - 1 - 181)) << "] = " << (num) << endl;
        }
    }

    ofstream file_rotations, file_scales, file_rotations2, file_scales2, file_metric_rotations, file_metric_scales;

    ostringstream key_rotations_fname, key_scales_fname,
            desc_rotations_fname, desc_scales_fname,
            metric_rotations_fname, metric_scales_fname;
    key_rotations_fname << "key_rotations_" << descnames[feature_no] << ".txt";
    key_scales_fname << "key_scales_" << descnames[feature_no] << ".txt";
    desc_rotations_fname << "desc_rotations_" << descnames[feature_no] << ".txt";
    desc_scales_fname << "desc_scales_" << descnames[feature_no] << ".txt";
    metric_rotations_fname << "metric_rotations_" << descnames[feature_no] << ".txt";
    metric_scales_fname << "metric_scales_" << descnames[feature_no] << ".txt";

    // Write rorations to file
    file_rotations.open(key_rotations_fname.str().c_str());
    file_rotations2.open(desc_rotations_fname.str().c_str());
    file_metric_rotations.open(metric_rotations_fname.str().c_str());
    for (int i = 0; i < 181; i++) {
        file_rotations << (int) key_rotations[2 * i] << " " << key_rotations[2 * i + 1] << endl;
        file_rotations2 << (int) desc_rotations[2 * i] << " " << desc_rotations[2 * i + 1] << endl;
        file_metric_rotations << (int) metric_rotations[2 * i] << " " << metric_rotations[2 * i + 1] << endl;
    }
    file_rotations.close();
    file_rotations2.close();
    file_metric_rotations.close();

    // Write scales to file
    file_scales.open(key_scales_fname.str().c_str());
    file_scales2.open(desc_scales_fname.str().c_str());
    file_metric_scales.open(metric_scales_fname.str().c_str());
    for (int i = 0; i < max_scale; i++) {
        file_scales << (int) key_scales[2 * i] << " " << key_scales[2 * i + 1] << endl;
        file_scales2 << (int) desc_scales[2 * i] << " " << desc_scales[2 * i + 1] << endl;
        file_metric_scales << (int) metric_scales[2 * i] << " " << metric_scales[2 * i + 1] << endl;
    }
    file_scales.close();
    file_scales2.close();
    file_metric_scales.close();

    waitKey();

    return 0;
}
